/*
Copyright 2019 FXcoder

This file is part of ScrollMaster.

ScrollMaster is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ScrollMaster is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with ScrollMaster. If not, see
http://www.gnu.org/licenses/.
*/

#property copyright "ScrollMaster 1.2.2. © FXcoder"
#property link      "https://fxcoder.blogspot.com"
#property strict

/*
Прокрутка текущего графика будет также приводить к прокрутке остальных (несвёрнутых) графиков.
Скрипт отключает автопрокрутку и сдвиг у всех таких графиков и возвращает их на место после выхода.
*/

//#define DEBUG

#include "ScrollMaster-include/bsl.mqh"
#include "ScrollMaster-include/s.mqh"
#include "ScrollMaster-include/class/timer.mqh"


class CScrollMasterChartInfo: public CBUncopyable
{
private:

	long id_;
	bool autoscroll_;
	bool shift_;


public:

	void CScrollMasterChartInfo(const CBChart &chart):
		id_(chart.id()), autoscroll_(chart.autoscroll()), shift_(chart.shift())
	{
	}

	_GET(long, id)
	_GET(bool, autoscroll)
	_GET(bool, shift)

};

void OnStart()
{
#ifdef __MQL4__
	string save_comment = _chart.comment();
	_chart.comment(_mql.program_name() + " is running");
#endif

	// Отключить автопрокрутку и сдвиг у всех несвёрнутых графиков.
	// Запомнить эти параметры для дальнейшего восстановления.

	CBArrayPtrT<CScrollMasterChartInfo> charts_info;

	for (CBChartX chart; chart.loop_real(); )
	{
		if (chart.is_minimized(false))
			continue;

		charts_info.add(new CScrollMasterChartInfo(chart));
		chart.autoscroll(false);
		chart.shift(false);
		chart.redraw();
	}

	// Ждать в цикле, пока пользователь пролистает основной (этот) график,
	// и затем прокрутить остальные графики на это же место.

	int charts_count = charts_info.size();
	
	// Выход по Esc не работает в 4 и иногда не срабатывает в 5, поэтому здесь альтернативный способ выхода - двойное нажатие End
	const uint dbl_press_max_ms = 555;
	const uint dbl_press_min_ms = 100; // против дребезга
	CTimer end_timer(dbl_press_max_ms);
	end_timer.stop();
	int end_press_count = 0;
	
	while (!IsStopped() && !_terminal.is_escape_key_pressed())
	{
		// check exit
		if (_terminal.is_end_key_pressed())
		{
			if (_math.is_in_range(end_timer.elapsed(), dbl_press_min_ms, dbl_press_max_ms) || end_timer.is_stopped())
				end_press_count++;

			if (end_press_count >= 2)
				break;
		
			end_timer.reset();
		}
		else
		{
			if (end_timer.check())
			{
				end_press_count = 0;
				end_timer.stop();
			}
		}

		// update position

		int master_pos = get_chart_pos(_chart);
		int master_bar = master_pos > 0 ? 0 : -master_pos;
		datetime master_time = _series.time(master_bar);
		
		for (int i = 0; i < charts_count; i++)
		{
			CBChart chart(charts_info[i].id());
		
			if (chart.is_this_chart())
				continue;
		
			CBSeries ser(chart.symbol(), chart.period());
		
			int pos = get_chart_pos(chart);
			int bar = pos > 0 ? 0 : -pos;
			datetime pos_time = ser.time(bar);
			
			if (pos_time != master_time)
			{
				int new_bar = ser.bar_shift(master_time);
				chart.navigate_end(-new_bar);
				//chart.redraw();
			}
		}

		// Без этого будет нагрузка на процессор, паузу выбрать по вкусу
		Sleep(33);
	}

	// Восстановить параметры графиков
	for (int i = 0; i < charts_count; i++)
	{
		CBChart chart(charts_info[i].id());
		chart.autoscroll(charts_info[i].autoscroll());
		chart.shift(charts_info[i].shift());
		
		if (charts_info[i].autoscroll())
			chart.navigate_end(0);
	}

#ifdef __MQL4__
	_chart.comment(save_comment);
#endif
}

int get_chart_pos(const CBChart &chart)
{
	return(chart.width_in_bars() - chart.first_visible_bar() - 2);
}

/*
История

1.2:
	* выход по Esc (только в 5) или по двойному нажатию End

1.1:
	* сохранение старого комментария в 4

1.0:
	* первая версия
*/
