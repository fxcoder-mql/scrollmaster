/*
Copyright 2019 FXcoder

This file is part of ScrollMaster.

ScrollMaster is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ScrollMaster is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with ScrollMaster. If not, see
http://www.gnu.org/licenses/.
*/

// Класс для доступа к свойствам MQL. Better Standard Library. © FXcoder

#property strict

#include "type/uncopyable.mqh"

class CBMQL: public CBUncopyable
{
public:

	static const bool is4;
	static const bool is5;

private:

	// Постоянные свойства
	bool   debug_;
	bool   frame_mode_;
	bool   optimization_;
	bool   profiler_;
	string program_path_;
	bool   tester_;
	bool   visual_mode_;
	ENUM_LICENSE_TYPE license_type_;
	ENUM_PROGRAM_TYPE program_type_;

#ifndef __MQL4__
	bool   forward_;
#endif


public:

	void CBMQL()
	{
		// Постоянные свойства получить сразу, сильно ускоряет последующий доступ
		
		debug_         = ::MQLInfoInteger(MQL_DEBUG);
		frame_mode_    = ::MQLInfoInteger(MQL_FRAME_MODE);
		optimization_  = ::MQLInfoInteger(MQL_OPTIMIZATION);
		profiler_      = ::MQLInfoInteger(MQL_PROFILER);
		program_path_  = ::MQLInfoString (MQL_PROGRAM_PATH);
		tester_        = ::MQLInfoInteger(MQL_TESTER);
		visual_mode_   = ::MQLInfoInteger(MQL_VISUAL_MODE);

		license_type_  = (ENUM_LICENSE_TYPE)::MQLInfoInteger(MQL_LICENSE_TYPE);
		program_type_  = (ENUM_PROGRAM_TYPE)::MQLInfoInteger(MQL_PROGRAM_TYPE);

#ifndef __MQL4__
		forward_ = ::MQLInfoInteger(MQL_FORWARD);
#endif
	}

	// Постоянные свойства (не меняются после запуска скрипта)
	bool              debug        () const { return( debug_        ); }  // Признак работы запущенной программы в режиме отладки
	bool              frame_mode   () const { return( frame_mode_   ); }  // Признак работы запущенного эксперта на графике в режиме сбора фреймов результатов оптимизации
	ENUM_LICENSE_TYPE license_type () const { return( license_type_ ); }  // Тип лицензии модуля EX5. Лицензия относится именно к тому модулю EX5, из которого делается запрос с помощью LicenseType.
	bool              profiler     () const { return( profiler_     ); }  // Признак работы запущенной программы в режиме профилирования кода
	string            program_path () const { return( program_path_ ); }  // Путь для данной запущенной программы
	ENUM_PROGRAM_TYPE program_type () const { return( program_type_ ); }  // Тип mql5-программы
	bool              tester       () const { return( tester_       ); }  // Признак работы запущенной программы в тестере (в т.ч. оптимизация)
	bool              optimization () const { return( optimization_ ); }  // Признак работы запущенной программы в процессе оптимизации
	bool              visual_mode  () const { return( visual_mode_  ); }  // Признак работы запущенной программы в визуальном режиме тестирования

	// Переменные свойства (могут меняться во время работы)
	bool              dlls_allowed    () const { return( (bool) ::MQLInfoInteger (MQL_DLLS_ALLOWED)    ); }  // (const?, проверить 4 и 5 отдельно) Разрешение на использование DLL для данной запущенной программы (проверить отдельно для сов. и индикаторов)
	int               memory_limit    () const { return(        ::MQLInfoInteger (MQL_MEMORY_LIMIT)    ); }  // (const? проверить при изменении памяти на ходу в виртуалке) Максимально возможный объём динамической памяти для MQL5-программы в MB
	int               memory_used     () const { return(        ::MQLInfoInteger (MQL_MEMORY_USED)     ); }  // Размер использованной памяти MQL5-программой в MB
	string            program_name    () const { return(        ::MQLInfoString  (MQL_PROGRAM_NAME)    ); }  // (const? проверить, меняет ли после изменения ShortName) Имя запущенной MQL5-программы
	bool              signals_allowed () const { return( (bool) ::MQLInfoInteger (MQL_SIGNALS_ALLOWED) ); }  // (const?) Разрешение на работу с сигналами данной запущенной программы
	bool              trade_allowed   () const { return( (bool) ::MQLInfoInteger (MQL_TRADE_ALLOWED)   ); }  // (const?) Разрешение на торговлю для данной запущенной программы

	// Универсальные функции доступа к свойствам
	int    info(ENUM_MQL_INFO_INTEGER property_id) const { return(::MQLInfoInteger(property_id)); }
	string info(ENUM_MQL_INFO_STRING  property_id) const { return(::MQLInfoString (property_id)); }

	// Дополнительные функции

	// true, когда необходимо показывать телеметрию (TesterChart и подобные)
	bool telemetry_mode() const
	{
		return(visual_mode_ && !profiler_);
	}

	int build() const { return(__MQLBUILD__); }


#ifndef __MQL4__

	// 5.1930
	bool forward() const { return(forward_); } // признак того, что программа находится в режиме форвард-тестирования

#endif

};

#ifdef __MQL4__
	const bool CBMQL::is4 = true;
#else
	const bool CBMQL::is4 = false;
#endif

#ifdef __MQL5__
	const bool CBMQL::is5 = true;
#else
	const bool CBMQL::is5 = false;
#endif

CBMQL _mql;
