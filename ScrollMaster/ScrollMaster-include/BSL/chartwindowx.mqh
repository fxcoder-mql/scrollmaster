/*
Copyright 2019 FXcoder

This file is part of ScrollMaster.

ScrollMaster is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ScrollMaster is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with ScrollMaster. If not, see
http://www.gnu.org/licenses/.
*/

// Класс окна графика. Better Standard Library. © FXcoder

//todo: разобраться с путаницей, какой класс сделать родителем. либо сделать один владельцем другого

#property strict

#include "windowx.mqh"

/*
Класс окна, содержащего окно графика.
Это окно содержит рамки и кнопки окна, а также окно-контейнер, в котором
расположен график.
*/
class CBChartWindowX : public CBWindowX
{
protected:

	const static int titled_style_;
	const static int titled_exstyle_;

	// запомнить объект окна-контейнера графика для особых команд (например, обновление)
	CBWindowX chart_window_;


public:

	void CBChartWindowX(long chart_id = 0):
		CBWindowX()
	{
		handle_ = int(::ChartGetInteger(chart_id, CHART_WINDOW_HANDLE));
		chart_window_.handle(handle_);
		
		// окно графика лежит внутри другого окна
		select_parent();
	}

	void refresh()
	{
		chart_window_.command(33324);
	}

	bool has_title()
	{
		return((style() & titled_style_) != 0);
	}

	void show_title()
	{
		style(style() | titled_style_);
		exstyle(exstyle() | titled_exstyle_);
	}

	void hide_title()
	{
		style(style() & ~titled_style_);
		exstyle(exstyle() & ~titled_exstyle_);
	}

};

// mki#40
const int CBChartWindowX::titled_style_   = CBChartWindowX::WS_DLGFRAME | CBChartWindowX::WS_THICKFRAME | CBChartWindowX::WS_BORDER;
const int CBChartWindowX::titled_exstyle_ = CBChartWindowX::WS_EX_WINDOWEDGE;

CBChartWindowX _chartwindowx;
