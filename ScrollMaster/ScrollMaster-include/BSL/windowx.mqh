/*
Copyright 2019 FXcoder

This file is part of ScrollMaster.

ScrollMaster is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ScrollMaster is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with ScrollMaster. If not, see
http://www.gnu.org/licenses/.
*/

// Класс окна Windows. Better Standard Library. © FXcoder

//todo: разобраться с путаницей, какой класс сделать родителем. либо сделать один владельцем другого

#property strict

#include "import/user32.mqh"
#include "window.mqh"

class CBWindowX: public CBWindow
{
protected:

	// const (win32 LONG ~ MQL int)
	const static int
		GA_PARENT, GA_ROOT,
		GWL_STYLE, GWL_EXSTYLE,
		WS_MINIMIZE, WS_MAXIMIZE, WS_DLGFRAME, WS_THICKFRAME, WS_BORDER,
		WS_EX_WINDOWEDGE,
		WM_KEYDOWN, WM_COMMAND,
		SW_MAXIMIZE, SW_MINIMIZE, SW_RESTORE;


public:

	void CBWindowX(): CBWindow(0) { }
	void CBWindowX(int handle): CBWindow(handle) { }


	bool select_parent()      { handle_ = this.parent_hadle();      return(this.is_valid()); }
	bool select_root_parent() { handle_ = this.root_parent_hadle(); return(this.is_valid()); }

	int parent_hadle()      const { return(user32::GetAncestor(handle_, GA_PARENT)); }
	int root_parent_hadle() const { return(user32::GetAncestor(handle_, GA_ROOT));   }

	bool is_maximized() const { return(this.check_style_flags(WS_MAXIMIZE)); }
	bool is_minimized() const { return(this.check_style_flags(WS_MINIMIZE)); }

	void maximize() const { user32::ShowWindow(handle_, SW_MAXIMIZE); }
	void minimize() const { user32::ShowWindow(handle_, SW_MINIMIZE); }
	void restore()  const { user32::ShowWindow(handle_, SW_RESTORE);  }

	void key_down(int key) const
	{
		user32::PostMessageA(handle_, WM_KEYDOWN, key, 0);
	}

	void command(int command) const
	{
		user32::PostMessageA(handle_, WM_COMMAND, command, 0);
	}

	int style() const
	{
		return(user32::GetWindowLongA(handle_, GWL_STYLE));
	}

	void style(int style) const
	{
		user32::SetWindowLongA(handle_, GWL_STYLE, style);
		update_after_style_change();
	}

	int exstyle() const
	{
		return(user32::GetWindowLongA(handle_, GWL_EXSTYLE));
	}

	void exstyle(int exstyle) const
	{
		user32::SetWindowLongA(handle_, GWL_EXSTYLE, exstyle);
		update_after_style_change();
	}


protected:

	// service methods

	void update_after_style_change() const
	{
		user32::SetWindowPos(handle_, 0, 0, 0, 0, 0, 39); // 39: SWP_FRAMECHANGED | SWP_NOZORDER | SWP_NOMOVE | SWP_NOSIZE
	}

	bool check_style_flags(int flags) const
	{
		return((style() & flags) != 0);
	}

};

// GetAncestor
const int CBWindowX::GA_PARENT     = 1;  // Retrieves the parent window. This does not include the owner, as it does with the GetParent function.
const int CBWindowX::GA_ROOT       = 2;  // Retrieves the root window by walking the chain of parent windows.

// Window Style
const int CBWindowX::GWL_STYLE    = -16;
const int CBWindowX::GWL_EXSTYLE  = -20;

const int CBWindowX::WS_MINIMIZE   = 0x20000000;
const int CBWindowX::WS_MAXIMIZE   = 0x01000000;
const int CBWindowX::WS_DLGFRAME   = 0x00400000;
const int CBWindowX::WS_BORDER     = 0x00800000;
const int CBWindowX::WS_THICKFRAME = 0x00040000;

const int CBWindowX::WS_EX_WINDOWEDGE = 0x00000100;

// Keys
const int CBWindowX::WM_KEYDOWN   = 0x0100;
const int CBWindowX::WM_COMMAND   = 0x0111;

// Show Window
const int CBWindowX::SW_MAXIMIZE  = 3;
const int CBWindowX::SW_MINIMIZE  = 6;
const int CBWindowX::SW_RESTORE   = 9;
