> **Этот проект закрыт. Обновления будут выходить только для MetaTrader 5 в новом проекте: [ScrollMaster-MT5](https://gitlab.com/fxcoder-mql/scrollmaster-mt5).**

# Скрипт ScrollMaster

Скрипт синхронизирует смещение всех графиков в терминале с тем, на котором он запущен. Может быть полезен как при обычном анализе, например при сравнении сигналов с разных инструментов и таймфреймов, так и при анализе самих инструментов анализа, сравнивая один и тот же индикатор или скрипт с разными параметрами.

![](media/scrollmaster2.gif)

Для полной функциональности в MT4 требуется разрешение DLL.

## См. также

В блоге: <https://www.fxcoder.ru/search/label/%7BScrollMaster%7D>

Установка: <https://www.fxcoder.ru/p/install-script.html>

:flag_gb:

Install: <https://www.fxcoder.ru/p/install-script-en.html>
